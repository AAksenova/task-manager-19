package ru.t1.aksenova.tm.api.service;

import ru.t1.aksenova.tm.enumerated.Status;
import ru.t1.aksenova.tm.model.Project;

public interface IProjectService extends IService<Project> {

    Project create(String name, String description);

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

    Project changeProjectStatusByIndex(Integer index, Status status);

    Project changeProjectStatusById(String id, Status status);

}
