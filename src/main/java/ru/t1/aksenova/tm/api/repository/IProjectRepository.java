package ru.t1.aksenova.tm.api.repository;

import ru.t1.aksenova.tm.model.Project;

public interface IProjectRepository extends IRepository<Project> {

    Project create(String name, String description);

}
