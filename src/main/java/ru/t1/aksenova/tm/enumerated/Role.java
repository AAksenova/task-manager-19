package ru.t1.aksenova.tm.enumerated;

public enum Role {

    USUAL("Usual user"),
    ADMIN("Administartor");

    private final String displayName;

    Role(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

}
