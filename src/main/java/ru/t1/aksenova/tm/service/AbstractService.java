package ru.t1.aksenova.tm.service;

import ru.t1.aksenova.tm.api.repository.IRepository;
import ru.t1.aksenova.tm.api.service.IService;
import ru.t1.aksenova.tm.enumerated.Sort;
import ru.t1.aksenova.tm.exception.entity.ModelNotFoundException;
import ru.t1.aksenova.tm.exception.field.IdEmptyException;
import ru.t1.aksenova.tm.exception.field.IndexIncorrectException;
import ru.t1.aksenova.tm.exception.field.ProjectIdEmptyException;
import ru.t1.aksenova.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    protected final R repository;

    public AbstractService(final R repository) {
        this.repository = repository;
    }

    @Override
    public M add(final M model) {
        if (model == null) throw new ModelNotFoundException();
        repository.add(model);
        return model;
    }

    @Override
    public List<M> findAll() {
        return repository.findAll();
    }

    @Override
    public List<M> findAll(final Comparator comparator) {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    @Override
    public List<M> findAll(final Sort sort) {
        if (sort == null) return findAll();
        return repository.findAll(sort);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public M findOneById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final M model = repository.findOneById(id);
        if (model == null) throw new ModelNotFoundException();
        return model;
    }

    @Override
    public M findOneByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        final M model = repository.findOneByIndex(index);
        if (model == null) throw new ModelNotFoundException();
        return model;
    }

    @Override
    public M remove(final M model) {
        if (model == null) throw new ModelNotFoundException();
        repository.remove(model);
        return model;
    }

    @Override
    public M removeById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final M model = repository.removeById(id);
        if (model == null) throw new ModelNotFoundException();
        return model;
    }

    @Override
    public M removeByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        final M model = repository.removeByIndex(index);
        if (model == null) throw new ModelNotFoundException();
        return model;
    }

    @Override
    public boolean existsById(final String id) {
        if (id == null || id.isEmpty()) throw new ProjectIdEmptyException();
        return repository.existsById(id);
    }

}
