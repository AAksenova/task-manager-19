package ru.t1.aksenova.tm.repository;

import ru.t1.aksenova.tm.api.repository.IProjectRepository;
import ru.t1.aksenova.tm.model.Project;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    public Project create(final String name, final String description) {
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        add(project);
        return project;
    }

}
