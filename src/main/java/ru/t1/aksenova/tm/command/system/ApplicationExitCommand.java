package ru.t1.aksenova.tm.command.system;

public final class ApplicationExitCommand extends AbstractSystemCommand {

    public static final String ARGUMENT = null;

    public static final String NAME = "exit";

    public static final String DESCRIPTION = "Close application.";

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}
