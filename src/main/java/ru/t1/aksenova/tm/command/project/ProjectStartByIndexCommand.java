package ru.t1.aksenova.tm.command.project;

import ru.t1.aksenova.tm.enumerated.Status;
import ru.t1.aksenova.tm.util.TerminalUtil;

public final class ProjectStartByIndexCommand extends AbstractProjectCommand {

    public static final String NAME = "project-start-by-index";

    public static final String DESCRIPTION = "Run project by index.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY INDEX]");
        System.out.println("ENTER PROJECT INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;

        getProjectService().changeProjectStatusByIndex(index, Status.IN_PROGRESS);
    }

}
