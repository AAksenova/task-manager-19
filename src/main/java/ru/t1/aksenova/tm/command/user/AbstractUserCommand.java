package ru.t1.aksenova.tm.command.user;

import ru.t1.aksenova.tm.api.service.IUserService;
import ru.t1.aksenova.tm.command.AbstractCommand;
import ru.t1.aksenova.tm.model.User;


public abstract class AbstractUserCommand extends AbstractCommand {

    protected IUserService getUserService() {
        return getServiceLocator().getUserService();
    }

    @Override
    public String getArgument() {
        return null;
    }

    protected void showUser(final User user) {
        if (user == null) return;
        System.out.println("USER ID: " + user.getId());
        System.out.println("USER LOGIN: " + user.getLogin());
        System.out.println("USER E-MAIL: " + user.getEmail());
        System.out.println("USER ROLE: " + user.getRole().getDisplayName());
    }

}
